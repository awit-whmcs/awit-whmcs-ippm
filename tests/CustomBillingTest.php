<?php
/*
 * AWIT IPPM - Custom billing test cases
 * Copyright (c) 2013-2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Set include path to AWIT IPPM module
set_include_path(get_include_path() . PATH_SEPARATOR . "/var/www/whmcs/modules/addons/awit_ippm");

// Load class
require_once("AWITWHMCS.php");

// Pull in awit_ippm module
require_once("awit_ippm.php");


class CustomBillingTest extends PHPUnit_Framework_TestCase
{
	protected static $harness;

	// Static items shared between all tests
	private static $harness_config;
	private static $harness_clients;
	private static $harness_products;
	private static $harness_productgroups;


	private static $harness_clientid;
	private static $harness_productgroupid;
	private static $harness_productid;
	private static $harness_productid2;
	private static $harness_product;
	private static $harness_product2;


	// We need to setup static items to share between classes
	public static function setUpBeforeClass()
	{
		echo "\n => setUpBeforeclass\n";
		self::$harness = new AWITWHMCS("awit_ippm");
		self::$harness_config = array();
		self::$harness_clients = array();
		self::$harness_products = array();
		self::$harness_productgroups = array();
	}


	protected function setUp()
	{
		// XXX: run before each test
	}


	public static function tearDownAfterClass()
	{
		echo "\n => TearDown\n";
		// If there were products created, remove them
		echo("Products: "); print_r(self::$harness_products); echo "\n";
		foreach (self::$harness_products as $pid) {
			echo "\n => Cleaning up product $pid\n";
			self::$harness->api_deleteProduct($pid);
		}

		// If there were product groups created, remove them
		echo("ProductGroups: "); print_r(self::$harness_productgroups); echo "\n";
		foreach (self::$harness_productgroups as $gid) {
			echo "\n => Cleaning up product group $gid\n";
			self::$harness->custom_deleteProductGroup($gid);
		}

		// If the client id is defined, remove it
		echo("Clients: "); print_r(self::$harness_clients); echo "\n";
		foreach (self::$harness_clients as $cid) {
			echo "\n => Cleaning up created client $cid\n";
			self::$harness->api_deleteClient($cid);
		}
	}


	public function testWHMCSSettings()
	{
		$this->assertEquals(self::$harness->custom_getConfig('DateFormat'),"YYYY-MM-DD","Date format must be 'YYYY-MM-DD'");
	}


	public function testIPPMModuleActive()
	{
		$this->assertTrue(self::$harness->custom_getConfigArrayItemExists('ActiveAddonModules','awit_ippm'),"IPPM module not active");

		// Initialize AWIT IPPM
		$config = awit_ippm_init();

		// Make sure it has 'version' set
		$this->assertArrayHasKey('version',$config,"IPPM module config did not return version");

		self::$harness_config = $config;
	}


	public function testCheckGatewayStatus()
	{
		$res = self::$harness->custom_getGatewayStatus('mailin');

		$this->assertEquals(1,$res,"Payment gateway 'mailin' must be enabled");
	}


	public function testIPPMModuleHooksActive()
	{
		$this->assertTrue(self::$harness->custom_getConfigArrayItemExists('AddonModulesHooks','awit_ippm'),"IPPM hooks not active");
	}


	public function testCreateClient()
	{
		$res = self::$harness->api_addClient(array(
			'firstname' => "Test",
			'lastname' => "Client",
			'email' => "nkukard+test@lbsd.net",
			'address1' => "Addr first line",
			'city' => "my city",
			'state' => "my state",
			'postcode' => "mypostcode",
			'country' => "US",
			'phonenumber' => "0123456789",
			'password2' => "testclient"
		));

		$this->assertGreaterThan(0,$res,"Failed to get client ID from client creation");

		echo "\n => Created client $res\n";

		array_push(self::$harness_clients,$res);
		self::$harness_clientid = $res;

		return $res;
	}


	public function testCreateProductGroup01()
	{
		$res = self::$harness->custom_addProductGroup(array(
			'name' => "Test Group",
			'order' => 99
		));

		$this->assertGreaterThan(0,$res,"Failed to get product group ID from product group creation");

		echo "\n => Created product group $res\n";

		array_push(self::$harness_productgroups,$res);
		self::$harness_productgroupid = $res;

		return $res;
	}


	public function testCreateProduct01()
	{
		$res_pid = self::$harness->api_addProduct(array(
			'type' => "other",
			'gid' => self::$harness_productgroupid,
			'name' => "Test product 01",
			'paytype' => "recurring",
			'pricing' => array(
					'1' => array(
						'monthly' => "1000",
						'quarterly' => "3000",
						'semiannually' => "5000"
					)
			 ),
			'tax' => true
		));
		$this->assertGreaterThan(0,$res_pid,"Failed to get product ID from product creation");
		echo "\n => Created product $res_pid\n";
		array_push(self::$harness_products,$res_pid);
		self::$harness_productid = $res_pid;

		$product = array(
			'pid' => $res_pid
		);

		// Grab global custom field mapping
		global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS;
		foreach (self::$harness_config['fields'] as $name => $field) {
			// We only want the billing attributes for this product
			if (!in_array($name,array(
					'signup_date',
					'billing_start','billing_end','billing_type','billing_period_start'
			))) {
				continue;
			}

			// If this is a AWIT IPPM custom field, try add the custom field
			if (isset($field['awitippm_ctype'])) {

				// Add product custom fields
				$res = self::$harness->custom_addProductCustomField($res_pid,array(
					'fieldname' => $AWIT_IPPM_CONFIG_CUSTOM_FIELDS[$name],
					'fieldtype' => "text",
					'description' => $AWIT_IPPM_CONFIG_CUSTOM_FIELDS[$name],
					'fieldoptions' => "",
					'regexpr' => "",
					'adminonly' => "on",
					'required' => "",
					'showorder' => "on",
					'showinvoice' => "",
					'sortorder' => "10"
				));

				printf("\n   => Created product custom field [%s] with name [%s] and pid [%s]\n",
					$name,
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS[$name],
					$res
				);

				// Set field ID
				$product['fields'][$name] = $res;

				$this->assertGreaterThan(0,$res,"Failed to add product custom field");
			}
		}

		self::$harness_product = $product;
	}


	public function testCreateProduct02()
	{
		$res_pid = self::$harness->api_addProduct(array(
			'type' => "other",
			'gid' => self::$harness_productgroupid,
			'name' => "Test product 02",
			'paytype' => "recurring",
			'pricing' => array(
					'1' => array(
						'monthly' => "10",
						'quarterly' => "30",
						'semiannually' => "50"
					)
			 ),
			'tax' => true
		));
		$this->assertGreaterThan(0,$res_pid,"Failed to get product ID from product creation");
		echo "\n => Created product $res_pid\n";
		array_push(self::$harness_products,$res_pid);
		self::$harness_productid2 = $res_pid;

		$product = array(
			'pid' => $res_pid
		);

		// Grab global custom field mapping
		global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS;
		foreach (self::$harness_config['fields'] as $name => $field) {
			// We only want the billing attributes for this product
			if (!in_array($name,array(
					'signup_date',
					'billing_start','billing_end','billing_type','billing_period_start',
					'variable_quantity', 'variable_amount', 'variable_identifier', 'variable_attributes',
					'commitment_value', 'commitment_calculation', 'bulk_discount_calculation'
			))) {
				continue;
			}

			// If this is a AWIT IPPM custom field, try add the custom field
			if (isset($field['awitippm_ctype'])) {

				// Add product custom fields
				$res = self::$harness->custom_addProductCustomField($res_pid,array(
					'fieldname' => $AWIT_IPPM_CONFIG_CUSTOM_FIELDS[$name],
					'fieldtype' => "text",
					'description' => $AWIT_IPPM_CONFIG_CUSTOM_FIELDS[$name],
					'fieldoptions' => "",
					'regexpr' => "",
					'adminonly' => "on",
					'required' => "",
					'showorder' => "on",
					'showinvoice' => "",
					'sortorder' => "10"
				));

				printf("\n   => Created product custom field [%s] with name [%s] and pid [%s]\n",
					$name,
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS[$name],
					$res
				);

				// Set field ID
				$product['fields'][$name] = $res;

				$this->assertGreaterThan(0,$res,"Failed to add product custom field");
			}
		}

		self::$harness_product2 = $product;
	}


	private function _billingTest($testcase)
	{
		printf("\n => Running test case: %s/%s\n",$testcase['id'],$testcase['name']);

		$res = self::$harness->api_addOrder(array(
			'clientid' => self::$harness_clientid,
			'pid' => $testcase['order']['product'],
			'paymentmethod' => 'mailin',
			'billingcycle' => $testcase['order']['billingcycle'],
			'domain' => "test.".$testcase['id'],
			'customfields' => base64_encode(serialize($testcase['customfields'])),
			'noemail' => 1,
			'noinvoiceemail' => 1,
		));
		$this->assertGreaterThan(0,$res,"Failed to create order for ".$testcase['id']);
		echo "\n => Created order $res\n";

		$invoiceID = self::$harness->custom_getInvoiceIDFromOrderID($res);
		$this->assertGreaterThan(0,$invoiceID,"Failed to get invoice ID from order ID ".$testcase['id']);

		$invoice = self::$harness->api_getInvoice($invoiceID);
		$invoiceItems = $invoice['items']['item'];

		$clientsProducts = self::$harness->api_getClientsProducts(self::$harness_clientid);

		$tests = $testcase['tests'];

		// Test invoice item count
		$numInvoiceItems = count($tests['line_items']);
		if ($numInvoiceItems > 0) {
			$this->assertCount(count($tests['line_items']),$invoiceItems,"Failed invoice item count for ".$testcase['id']);
		} else {
			$this->assertEquals(NULL,$invoiceItems,"Failed invoice item list should be NULL for ".$testcase['id']);
		}

		// Check if we testing invoice date
		if (isset($tests['invoice_date'])) {
			$this->assertEquals($tests['invoice_date'],$invoice['date'],"Invoice date should match [".
					$tests['invoice_date']."] for ".$testcase['id']);
		}

		// Check if we testing invoice due date
		if (isset($tests['invoice_duedate'])) {
			$this->assertEquals($tests['invoice_duedate'],$invoice['duedate'],"Invoice due date should match [".
					$tests['invoice_duedate']."] for ".$testcase['id']);
		}

		// Check if we testing tax
		if (isset($tests['invoice_tax'])) {
			$this->assertEquals($tests['invoice_tax'],$invoice['tax'],"Invoice tax amount should match [".
					$tests['invoice_tax']."] for ".$testcase['id']);
		}

		// Check if we testing invoice total
		if (isset($tests['invoice_subtotal'])) {
			$this->assertEquals($tests['invoice_subtotal'],$invoice['subtotal'],"Invoice subtotal should match [".
					$tests['invoice_subtotal']."] for ".$testcase['id']);
		}

		// Process all the asserts
		foreach ($tests['line_items'] as $itemNum => $item) {
			// Line item tests first
			if (isset($item['type'])) {
				$this->assertEquals($item['type'],$invoiceItems[$itemNum]['type'],"Invoice line item $itemNum type should be [".
						$item['type']."]");
			}
/* FIXME
			if (isset($item['relid'])) {
				$this->assertEquals($item['relid'],$invoiceItems[$itemNum]['relid'],"Invoice line item $itemNum relid should be [".
						$item['relid']."]");
			}
*/
			if (isset($item['description'])) {
				$this->assertContains($item['description'],$invoiceItems[$itemNum]['description'],
						"Invoice line item $itemNum description should contain [".$item['description']."]");
			}
			if (isset($item['amount'])) {
				$this->assertEquals($item['amount'],$invoiceItems[$itemNum]['amount'],"Invoice line item $itemNum amount should be [".
						$item['amount']."]");
			}
			if (isset($item['taxed'])) {
				$this->assertEquals($item['taxed'],$invoiceItems[$itemNum]['taxed'],"Invoice line item $itemNum taxed flag should be [".
						$item['taxed']."]");
			}
			// Finally client product tests
			if (isset($item['firstpaymentamount'])) {
				$this->assertEquals($item['firstpaymentamount'],$clientsProducts[$invoiceItems[$itemNum]['relid']]['firstpaymentamount'],"Invoice line item $itemNum firstpaymentamount should be [".
						$item['firstpaymentamount']."]");
			}
			if (isset($item['nextduedate'])) {
				$this->assertEquals($item['nextduedate'],$clientsProducts[$invoiceItems[$itemNum]['relid']]['nextduedate'],"Invoice line item $itemNum nextduedate should be [".
						$item['nextduedate']."]");
			}
			if (isset($item['regdate'])) {
				$this->assertEquals($item['regdate'],$clientsProducts[$invoiceItems[$itemNum]['relid']]['regdate'],"Invoice line item $itemNum regdate should be [".
						$item['regdate']."]");
			}
			if (isset($item['status'])) {
				$this->assertEquals($item['status'],$clientsProducts[$invoiceItems[$itemNum]['relid']]['status'],"Invoice line item $itemNum status should be [".
						$item['status']."]");
			}
		}

	}


	/**
	 * @group basic
	 */
	public function testBillingTest010()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "010",
			'name' => "Normal prepaid billing",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "REG2009-04-01",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				// We not going to check invoice_date or invoice_due
				// These normally depend on the   (NextDueDate - InvoicingGeneration)
				/**
				'invoice_date' => "xxxx-xx-xx",
				 */
				'invoice_duedate' => "2010-03-31",
				 // Invoice total without tax
				'invoice_subtotal' => "1000",
				// Invoice tax amount
				'invoice_tax' => "140",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2009-04-01",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "1000",
						'description' => "Prepaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					)
				),
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group basic
	 */
	public function testBillingTest012()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "012",
			'name' => "Normal prepaid billing 6 month",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "semiannually",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "REG2009-04-01",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				// We not going to check invoice_date or invoice_due
				// These normally depend on the   (NextDueDate - InvoicingGeneration)
				/**
				'invoice_date' => "xxxx-xx-xx",
				 */
				'invoice_duedate' => "2010-03-31",
				 // Invoice total without tax
				'invoice_subtotal' => "5000",
				// Invoice tax amount
				'invoice_tax' => "700",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2009-04-01",
						'nextduedate' => "2010-10-01",
						'firstpaymentamount' => "5000",
						'description' => "Prepaid: 2010-04-01 to 2010-09-30",
						'amount' => "5000",
						'taxed' => 1
					)
				),
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group basic
	 */
	public function testBillingTest020()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "020",
			'name' => "Normal current billing",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "REG2009-04-01",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				// We not going to check invoice_date or invoice_due
				// These normally depend on the   (NextDueDate - InvoicingGeneration)
				/**
				'invoice_date' => "xxxx-xx-xx",
				 */
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "1000",
				// Invoice tax amount
				'invoice_tax' => "140",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2009-04-01",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "1000",
						'description' => "Current: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					)
				),
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group basic
	 */
	public function testBillingTest022()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "022",
			'name' => "Normal current billing 6 month",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "semiannually",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "REG2009-04-01",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				// We not going to check invoice_date or invoice_due
				// These normally depend on the   (NextDueDate - InvoicingGeneration)
				/**
				'invoice_date' => "xxxx-xx-xx",
				 */
				'invoice_duedate' => "2010-03-31",
				 // Invoice total without tax
				'invoice_subtotal' => "5000",
				// Invoice tax amount
				'invoice_tax' => "700",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2009-04-01",
						'nextduedate' => "2010-10-01",
						'firstpaymentamount' => "5000",
						'description' => "Current: 2010-03-01 to 2010-08-31",
						'amount' => "5000",
						'taxed' => 1
					)
				),
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group basic
	 */
	public function testBillingTest030()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "030",
			'name' => "Normal postpaid billing",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "REG2009-04-01",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				// We not going to check invoice_date or invoice_due
				// These normally depend on the   (NextDueDate - InvoicingGeneration)
				/**
				'invoice_date' => "xxxx-xx-xx",
				 */
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "1000",
				// Invoice tax amount
				'invoice_tax' => "140",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2009-04-01",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "1000",
						'description' => "Postpaid: 2010-02-01 to 2010-02-28",
						'amount' => "1000",
						'taxed' => 1
					)
				),
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group basic
	 */
	public function testBillingTest032()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "032",
			'name' => "Normal postpaid billing 6 month",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "semiannually",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "REG2009-04-01",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				// We not going to check invoice_date or invoice_due
				// These normally depend on the   (NextDueDate - InvoicingGeneration)
				/**
				'invoice_date' => "xxxx-xx-xx",
				 */
				'invoice_duedate' => "2010-03-31",
				 // Invoice total without tax
				'invoice_subtotal' => "5000",
				// Invoice tax amount
				'invoice_tax' => "700",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2009-04-01",
						'nextduedate' => "2010-10-01",
						'firstpaymentamount' => "5000",
						'description' => "Postpaid: 2009-09-01 to 2010-02-28",
						'amount' => "5000",
						'taxed' => 1
					)
				),
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest100()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "100",
			'name' => "Prepaid signup on billing date, no pro-rata, no included days, pay immediately",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-04-01",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-04-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-04-01",
				'invoice_duedate' => "2010-04-01",
				// Invoice total without tax
				'invoice_subtotal' => "1000",
				// Invoice tax amount
				'invoice_tax' => "140",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-04-01",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "1000",
						'description' => "Prepaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest101()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "101",
			'name' => "Prepaid signup after billing start date, no pro-rata, before included days, pay by end of cycle",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-20",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-03-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-20",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "2000",
				// Invoice tax amount
				'invoice_tax' => "280",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-20",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "2000",
						'description' => "Prepaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest102()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "102",
			'name' => "Prepaid signup after billing start date, pro-rata, before included days, pay by end of cycle",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-25",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-03-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-25",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "1387.10",
				// Invoice tax amount
				'invoice_tax' => "194.19",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-25",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "1387.10",
						'description' => "Prepaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-20 to 2010-03-31",
						'amount' => "387.10",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest103()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "103",
			'name' => "Prepaid signup after billing start date, pro-rata, before included days, pay by end of cycle, 2 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-25",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-02-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-25",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "2321.43",
				// Invoice tax amount
				'invoice_tax' => "325",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-25",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "2321.43",
						'description' => "Prepaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-02-20 to 2010-02-28",
						'amount' => "321.43",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest104()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "104",
			'name' => "Prepaid signup after billing start date, pro-rata, after included days, pay by end of cycle",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-03-15",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "1548.39",
				// Invoice tax amount
				'invoice_tax' => "216.77",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "1548.39",
						'description' => "Prepaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-15 to 2010-03-31",
						'amount' => "548.39",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest105()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "105",
			'name' => "Prepaid signup after billing start date, pro-rata, after included days, pay by end of cycle, 2 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-02-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "2321.43",
				// Invoice tax amount
				'invoice_tax' => "325",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "2321.43",
						'description' => "Prepaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-02-20 to 2010-02-28",
						'amount' => "321.43",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest110()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "110",
			'name' => "Prepaid signup on billing start date, pay by end of cycle",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-04-01",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-04-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-04-01",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "2000",
				// Invoice tax amount
				'invoice_tax' => "280",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-04-01",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "2000",
						'description' => "Prepaid: 2010-05-01 to 2010-05-31",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest111()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "111",
			'name' => "Prepaid signup after billing start date, no pro-rata, before included days, pay by end of cycle",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-20",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-03-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-20",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "3000",
				// Invoice tax amount
				'invoice_tax' => "420",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-20",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "3000",
						'description' => "Prepaid: 2010-05-01 to 2010-05-31",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest112()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "112",
			'name' => "Prepaid signup after billing start date, pro-rata, before included days, pay by end of cycle",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-25",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-03-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-25",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "2387.10",
				// Invoice tax amount
				'invoice_tax' => "334.19",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-25",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "2387.10",
						'description' => "Prepaid: 2010-05-01 to 2010-05-31",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-20 to 2010-03-31",
						'amount' => "387.10",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest113()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "113",
			'name' => "Prepaid signup after billing start date, pro-rata, before included days, pay by end of cycle, 2 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-25",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-02-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-25",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "3321.43",
				// Invoice tax amount
				'invoice_tax' => "465",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-25",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "3321.43",
						'description' => "Prepaid: 2010-05-01 to 2010-05-31",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-02-20 to 2010-02-28",
						'amount' => "321.43",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest114()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "114",
			'name' => "Prepaid signup after billing start date, pro-rata, before included days, pay by end of cycle",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-03-15",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "2548.39",
				// Invoice tax amount
				'invoice_tax' => "356.77",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "2548.39",
						'description' => "Prepaid: 2010-05-01 to 2010-05-31",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-15 to 2010-03-31",
						'amount' => "548.39",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest115()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "115",
			'name' => "Prepaid signup after billing start date, pro-rata, before included days, pay by end of cycle, 2 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-02-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "3321.43",
				// Invoice tax amount
				'invoice_tax' => "465",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "3321.43",
						'description' => "Prepaid: 2010-05-01 to 2010-05-31",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-02-20 to 2010-02-28",
						'amount' => "321.43",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest121()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "121",
			'name' => "Prepaid signup before billing start date, pro-rata, before included days, pay by end of cycle",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-20",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-03-23",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-20",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "1290.32",
				// Invoice tax amount
				'invoice_tax' => "180.64",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-20",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "1290.32",
						'description' => "Prepaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-23 to 2010-03-31",
						'amount' => "290.32",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest122()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "122",
			'name' => "Prepaid signup before billing start date, pro-rata, before included days, pay by end of cycle",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-25",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-03-26",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-25",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "1193.55",
				// Invoice tax amount
				'invoice_tax' => "167.10",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-25",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "1193.55",
						'description' => "Prepaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-26 to 2010-03-31",
						'amount' => "193.55",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest123()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "123",
			'name' => "Prepaid signup before billing start date, pro-rata, before included days, pay by end of cycle, 2 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-02-20",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-02-25",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-02-20",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "2142.86",
				// Invoice tax amount
				'invoice_tax' => "300",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-02-20",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "2142.86",
						'description' => "Prepaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-02-25 to 2010-02-28",
						'amount' => "142.86",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest124()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "124",
			'name' => "Prepaid signup before billing start date, pro-rata, before included days, pay by end of cycle",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-02-20",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-03-07",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-02-20",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "1806.45",
				// Invoice tax amount
				'invoice_tax' => "252.90",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-02-20",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "1806.45",
						'description' => "Prepaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-07 to 2010-03-31",
						'amount' => "806.45",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest125()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "125",
			'name' => "Prepaid signup before billing start date, pro-rata, after included days, pay by end of cycle",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-03-29",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "1096.77",
				// Invoice tax amount
				'invoice_tax' => "153.55",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "1096.77",
						'description' => "Prepaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-29 to 2010-03-31",
						'amount' => "96.77",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest126()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "126",
			'name' => "Prepaid signup before billing start date, pro-rata, after included days, pay by end of cycle",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-04-02",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "966.67",
				// Invoice tax amount
				'invoice_tax' => "135.33",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "966.67",
						'description' => "Prepaid: 2010-04-02 to 2010-04-30",
						'amount' => "966.67",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest130()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "130",
			'name' => "Prepaid signup after billing period start date, after billing start date, pro-rata, after included days, pay immediately",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-04-22",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-04-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-04-22",
				'invoice_duedate' => "2010-04-22",
				// Invoice total without tax
				'invoice_subtotal' => "366.67",
				// Invoice tax amount
				'invoice_tax' => "51.33",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-04-22",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "366.67",
						'description' => "Prepaid: 2010-04-20 to 2010-04-30",
						'amount' => "366.67",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest131()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "131",
			'name' => "Prepaid signup after billing period start date, before billing start date, pro-rata, after included days",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-04-18",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-05-05",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
//FIXME
				/**
				'invoice_date' => "2010-04-18",
				'invoice_duedate' => "2010-04-18",
				*/
				// Invoice total without tax
				'invoice_subtotal' => "0",
				// Invoice tax amount
				'invoice_tax' => "0",
				'line_items' => array(
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest132()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "132",
			'name' => "Prepaid signup after billing period start date, before billing start date, pro-rata, after included days",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-04-18",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-06-10",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
//FIXME
				/**
				'invoice_date' => "2010-04-18",
				'invoice_duedate' => "2010-04-18",
				*/
				// Invoice total without tax
				'invoice_subtotal' => "0",
				// Invoice tax amount
				'invoice_tax' => "0",
				'line_items' => array(
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest140()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "140",
			'name' => "Prepaid6 signup after billing start date, pro-rata, after included days, pay by end of cycle",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "quarterly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2009-08-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "10369.57",
				// Invoice tax amount
				'invoice_tax' => "1451.74",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-07-01",
						'firstpaymentamount' => "10369.57",
						'description' => "Prepaid: 2010-04-01 to 2010-06-30",
						'amount' => "3000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2009-08-20 to 2009-09-30",
						'amount' => "1369.57",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2009-10-01 to 2009-12-31",
						'amount' => "3000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-01-01 to 2010-03-31",
						'amount' => "3000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group prepaid
	 */
	public function testBillingTest150()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "150",
			'name' => "Prepaid6 signup after billing start date, pro-rata, after included days, pay by end of cycle, billing end before cycle end",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "quarterly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2009-08-20",
				$cfields['billing_end'] => "2010-05-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "8984.95",
				// Invoice tax amount
				'invoice_tax' => "1257.89",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-04-01",
						'firstpaymentamount' => "8984.95",
						'status' => "Cancelled",
						'description' => "Prepaid: 2010-04-01 to 2010-05-20",
						'amount' => "1615.38",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2009-08-20 to 2009-09-30",
						'amount' => "1369.57",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2009-10-01 to 2009-12-31",
						'amount' => "3000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-01-01 to 2010-03-31",
						'amount' => "3000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest200()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "200",
			'name' => "Current signup on billing date",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-04-01",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-04-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
//FIXME
				/**
				 'invoice_date' => "2010-04-01",
				 'invoice_duedate' => "2010-04-01",
				 */
				// Invoice total without tax
				'invoice_subtotal' => "0",
				// Invoice tax amount
				'invoice_tax' => "0",
				'line_items' => array(
				),
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest201()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "201",
			'name' => "Current signup after billing start date, no pro-rata, before included days, pay by next run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-20",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-03-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-20",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "1000",
				// Invoice tax amount
				'invoice_tax' => "140",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-20",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "1000",
						'description' => "Current: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest202()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "202",
			'name' => "Current signup after billing start date, pro-rata, before included days, pay by next run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-25",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-03-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-25",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "387.10",
				// Invoice tax amount
				'invoice_tax' => "54.19",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-25",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "387.10",
						'description' => "Current: 2010-03-20 to 2010-03-31",
						'amount' => "387.10",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest203()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "203",
			'name' => "Current signup after billing start date, pro-rata, before included days, pay by next run, 2 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-25",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-02-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-25",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "1321.43",
				// Invoice tax amount
				'invoice_tax' => "185",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-25",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "1321.43",
						'description' => "Current: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-02-20 to 2010-02-28",
						'amount' => "321.43",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest204()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "204",
			'name' => "Current signup after billing start date, pro-rata, after included days, pay by next run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-03-15",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "548.39",
				// Invoice tax amount
				'invoice_tax' => "76.77",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "548.39",
						'description' => "Current: 2010-03-15 to 2010-03-31",
						'amount' => "548.39",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest205()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "205",
			'name' => "Current signup after billing start date, pro-rata, after included days, pay by next run, 2 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-02-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "1321.43",
				// Invoice tax amount
				'invoice_tax' => "185",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "1321.43",
						'description' => "Current: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-02-20 to 2010-02-28",
						'amount' => "321.43",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest210()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "210",
			'name' => "Current signup on billing start date, pay by next run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-04-01",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-04-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-04-01",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "1000",
				// Invoice tax amount
				'invoice_tax' => "140",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-04-01",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "1000",
						'description' => "Current: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest211()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "211",
			'name' => "Current signup after billing start date, no pro-rata, before included days, pay by next run, 2 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-20",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-03-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-20",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "2000",
				// Invoice tax amount
				'invoice_tax' => "280",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-20",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "2000",
						'description' => "Current: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest212()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "212",
			'name' => "Current signup after billing start date, pro-rata, before included days, pay by next run, 2 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-25",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-03-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-25",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "1387.10",
				// Invoice tax amount
				'invoice_tax' => "194.19",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-25",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "1387.10",
						'description' => "Current: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-20 to 2010-03-31",
						'amount' => "387.10",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest213()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "213",
			'name' => "Current signup before billing date, pro-rata, before included days, pay by next run, 3 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-25",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-02-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-25",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "2321.43",
				// Invoice tax amount
				'invoice_tax' => "325",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-25",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "2321.43",
						'description' => "Current: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-02-20 to 2010-02-28",
						'amount' => "321.43",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest214()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "214",
			'name' => "Current signup after billing start date, pro-rata, before included days",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-03-15",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "1548.39",
				// Invoice tax amount
				'invoice_tax' => "216.77",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "1548.39",
						'description' => "Current: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-15 to 2010-03-31",
						'amount' => "548.39",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest215()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "215",
			'name' => "Current signup after billing start date, pro-rata, before included days, pay by next run, 3 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-02-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "2321.43",
				// Invoice tax amount
				'invoice_tax' => "325",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "2321.43",
						'description' => "Current: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-02-20 to 2010-02-28",
						'amount' => "321.43",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest221()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "221",
			'name' => "Current signup before billing start date, pro-rata, before included days, pay by next run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-20",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-03-23",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-20",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "290.32",
				// Invoice tax amount
				'invoice_tax' => "40.64",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-20",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "290.32",
						'description' => "Current: 2010-03-23 to 2010-03-31",
						'amount' => "290.32",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest222()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "222",
			'name' => "Current signup before billing start date, pro-rata, before included days, pay by next run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-25",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-03-26",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-25",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "193.55",
				// Invoice tax amount
				'invoice_tax' => "27.10",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-25",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "193.55",
						'description' => "Current: 2010-03-26 to 2010-03-31",
						'amount' => "193.55",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest223()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "223",
			'name' => "Current signup before billing start date, pro-rata, before included days, pay by next run, 2 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-02-20",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-02-25",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-02-20",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "1142.86",
				// Invoice tax amount
				'invoice_tax' => "160",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-02-20",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "1142.86",
						'description' => "Current: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-02-25 to 2010-02-28",
						'amount' => "142.86",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest224()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "224",
			'name' => "Current signup before billing start date, pro-rata, before included days, pay by next run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-02-20",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-03-07",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-02-20",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "806.45",
				// Invoice tax amount
				'invoice_tax' => "112.90",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-02-20",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "806.45",
						'description' => "Current: 2010-03-07 to 2010-03-31",
						'amount' => "806.45",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest225()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "225",
			'name' => "Current signup before billing start date, pro-rata, after included days, pay by next run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-03-29",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "96.77",
				// Invoice tax amount
				'invoice_tax' => "13.55",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "96.77",
						'description' => "Current: 2010-03-29 to 2010-03-31",
						'amount' => "96.77",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest226()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "226",
			'name' => "Current signup before billing start date, pro-rata, after included days, pay by next run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-04-02",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
//FIXME
				/**
				 'invoice_date' => "2010-03-26",
				 'invoice_duedate' => "2010-03-31",
				 */
				// Invoice total without tax
				'invoice_subtotal' => "0",
				// Invoice tax amount
				'invoice_tax' => "0",
				'line_items' => array(
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest230()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "230",
			'name' => "Current signup after billing period start date, after billing start date, pro-rata, after included days, pay by next run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-04-22",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-04-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
//FIXME
				/**
				 'invoice_date' => "2010-04-20",
				 'invoice_duedate' => "2010-04-30",
				 */
				// Invoice total without tax
				'invoice_subtotal' => "0",
				// Invoice tax amount
				'invoice_tax' => "0",
				'line_items' => array(
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest231()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "231",
			'name' => "Current signup after billing period start date, before billing start date, pro-rata, after included days, pay by next run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-04-18",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-05-05",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
//FIXME
				/**
				 'invoice_date' => "2010-04-20",
				 'invoice_duedate' => "2010-04-30",
				 */
				// Invoice total without tax
				'invoice_subtotal' => "0",
				// Invoice tax amount
				'invoice_tax' => "0",
				'line_items' => array(
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest232()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "232",
			'name' => "Current signup after billing period start date, before billing start date, pro-rata, after included days, pay by next run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-04-18",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2010-06-10",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
//FIXME
				/**
				 'invoice_date' => "2010-04-18",
				 'invoice_duedate' => "2010-04-30",
				 */
				// Invoice total without tax
				'invoice_subtotal' => "0",
				// Invoice tax amount
				'invoice_tax' => "0",
				'line_items' => array(
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest240()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "240",
			'name' => "Current6 signup after billing start date, pro-rata, after included days, pay by end of cycle",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "quarterly"
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2009-08-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "9391.30",
				// Invoice tax amount
				'invoice_tax' => "1314.78",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-07-01",
						'firstpaymentamount' => "9391.30",
						'description' => "Current: 2010-03-01 to 2010-05-31",
						'amount' => "3000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2009-08-20 to 2009-08-31",
						'amount' => "391.30",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2009-09-01 to 2009-11-30",
						'amount' => "3000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2009-12-01 to 2010-02-28",
						'amount' => "3000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group current
	 */
	public function testBillingTest250()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "250",
			'name' => "Current6 signup after billing start date, pro-rata, after included days, pay by end of cycle, billing end before cycle end",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "quarterly"
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "current",
				$cfields['billing_start'] => "2009-08-20",
				$cfields['billing_end'] => "2010-05-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "9000",
				// Invoice tax amount
				'invoice_tax' => "1260.00",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-04-01",
						'firstpaymentamount' => "9000",
						'status' => "Cancelled",
						'description' => "Current: 2010-03-01 to 2010-05-20",
						'amount' => "2608.70",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2009-08-20 to 2009-08-31",
						'amount' => "391.30",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2009-09-01 to 2009-11-30",
						'amount' => "3000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2009-12-01 to 2010-02-28",
						'amount' => "3000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest300()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "200",
			'name' => "Postpaid signup on billing day, pay by next run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-04-01",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-04-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
//FIXME
				/**
				 'invoice_date' => "2010-04-01",
				 'invoice_duedate' => "2010-04-01",
				 */
				// Invoice total without tax
				'invoice_subtotal' => "0",
				// Invoice tax amount
				'invoice_tax' => "0",
				'line_items' => array(
				),
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest301()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "301",
			'name' => "Postpaid signup after billing start date, no pro-rata, before included days, pay by next run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-20",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-03-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-20",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "1000",
				// Invoice tax amount
				'invoice_tax' => "140",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-20",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "1000",
						'description' => "Postpaid: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest302()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "302",
			'name' => "Postpaid signup after billing start date, pro-rata, before included days, pay by next run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-25",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-03-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-25",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "387.10",
				// Invoice tax amount
				'invoice_tax' => "54.19",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-25",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "387.10",
						'description' => "Postpaid: 2010-03-20 to 2010-03-31",
						'amount' => "387.10",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest303()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "303",
			'name' => "Postpaid signup after billing start date, pro-rata, before included days, pay by end of run, 2 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-25",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-02-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-25",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "1321.43",
				// Invoice tax amount
				'invoice_tax' => "185",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-25",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "1321.43",
						'description' => "Postpaid: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-02-20 to 2010-02-28",
						'amount' => "321.43",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest304()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "304",
			'name' => "Postpaid signup after billing start date, pro-rata, after included days, pay by end of run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-03-15",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "548.39",
				// Invoice tax amount
				'invoice_tax' => "76.77",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "548.39",
						'description' => "Postpaid: 2010-03-15 to 2010-03-31",
						'amount' => "548.39",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest305()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "305",
			'name' => "Postpaid signup after billing start date, pro-rata, after included days, pay by end of run, 2 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-02-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "1321.43",
				// Invoice tax amount
				'invoice_tax' => "185",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "1321.43",
						'description' => "Postpaid: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-02-20 to 2010-02-28",
						'amount' => "321.43",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest310()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "310",
			'name' => "Postpaid signup on billing start date, pay by end of run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-04-01",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-04-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-06-01"
			),
			'tests' => array(
				'invoice_date' => "2010-04-01",
				'invoice_duedate' => "2010-05-31",
				// Invoice total without tax
				'invoice_subtotal' => "1000",
				// Invoice tax amount
				'invoice_tax' => "140",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-04-01",
						'nextduedate' => "2010-07-01",
						'firstpaymentamount' => "1000",
						'description' => "Postpaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest311()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "311",
			'name' => "Postpaid signup after billing start date, no pro-rata, before included days, pay by end of run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-20",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-03-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-06-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-20",
				'invoice_duedate' => "2010-05-31",
				// Invoice total without tax
				'invoice_subtotal' => "2000",
				// Invoice tax amount
				'invoice_tax' => "280",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-20",
						'nextduedate' => "2010-07-01",
						'firstpaymentamount' => "2000",
						'description' => "Postpaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest312()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "312",
			'name' => "Postpaid signup after billing start date, pro-rata, before included days, pay by end of run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-25",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-03-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-06-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-25",
				'invoice_duedate' => "2010-05-31",
				// Invoice total without tax
				'invoice_subtotal' => "1387.10",
				// Invoice tax amount
				'invoice_tax' => "194.19",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-25",
						'nextduedate' => "2010-07-01",
						'firstpaymentamount' => "1387.10",
						'description' => "Postpaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-20 to 2010-03-31",
						'amount' => "387.10",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest313()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "313",
			'name' => "Postpaid signup before billing date, pro-rata, before included days, pay by end of run, 2 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-25",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-02-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-06-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-25",
				'invoice_duedate' => "2010-05-31",
				// Invoice total without tax
				'invoice_subtotal' => "2321.43",
				// Invoice tax amount
				'invoice_tax' => "325",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-25",
						'nextduedate' => "2010-07-01",
						'firstpaymentamount' => "2321.43",
						'description' => "Postpaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-02-20 to 2010-02-28",
						'amount' => "321.43",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest314()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "314",
			'name' => "Postpaid signup after billing start date, pro-rata, before included days, pay by end of run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-03-15",
				// Invoice date
				$cfields['billing_period_start'] => "2010-06-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-05-31",
				// Invoice total without tax
				'invoice_subtotal' => "1548.39",
				// Invoice tax amount
				'invoice_tax' => "216.77",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-07-01",
						'firstpaymentamount' => "1548.39",
						'description' => "Postpaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-15 to 2010-03-31",
						'amount' => "548.39",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest315()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "315",
			'name' => "Postpaid signup after billing start date, pro-rata, before included days, pay by next run, 2 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-02-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-06-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-05-31",
				// Invoice total without tax
				'invoice_subtotal' => "2321.43",
				// Invoice tax amount
				'invoice_tax' => "325",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-07-01",
						'firstpaymentamount' => "2321.43",
						'description' => "Postpaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-02-20 to 2010-02-28",
						'amount' => "321.43",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest321()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "321",
			'name' => "Postpaid signup before billing start date, pro-rata, before included days, pay by run end",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-20",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-03-23",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-20",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "290.32",
				// Invoice tax amount
				'invoice_tax' => "40.64",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-20",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "290.32",
						'description' => "Postpaid: 2010-03-23 to 2010-03-31",
						'amount' => "290.32",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest322()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "322",
			'name' => "Postpaid signup before billing start date, pro-rata, before included days, pay by end of run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-25",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-03-26",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-25",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "193.55",
				// Invoice tax amount
				'invoice_tax' => "27.10",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-25",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "193.55",
						'description' => "Postpaid: 2010-03-26 to 2010-03-31",
						'amount' => "193.55",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest323()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "323",
			'name' => "Postpaid signup before billing start date, pro-rata, before included days, pay by end of run, 2 periods",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-02-20",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-02-25",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-02-20",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "1142.86",
				// Invoice tax amount
				'invoice_tax' => "160",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-02-20",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "1142.86",
						'description' => "Postpaid: 2010-03-01 to 2010-03-31",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-02-25 to 2010-02-28",
						'amount' => "142.86",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest324()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "324",
			'name' => "Postpaid signup before billing start date, pro-rata, before included days, pay by end of run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-02-20",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-03-07",
				// Invoice date
				$cfields['billing_period_start'] => "2010-06-01"
			),
			'tests' => array(
				'invoice_date' => "2010-02-20",
				'invoice_duedate' => "2010-05-31",
				// Invoice total without tax
				'invoice_subtotal' => "1806.45",
				// Invoice tax amount
				'invoice_tax' => "252.90",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-02-20",
						'nextduedate' => "2010-07-01",
						'firstpaymentamount' => "1806.45",
						'description' => "Postpaid: 2010-04-01 to 2010-04-30",
						'amount' => "1000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2010-03-07 to 2010-03-31",
						'amount' => "806.45",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest325()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "325",
			'name' => "Postpaid signup before billing start date, pro-rata, after included days, pay by end of run",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-03-29",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "96.77",
				// Invoice tax amount
				'invoice_tax' => "13.55",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "96.77",
						'description' => "Postpaid: 2010-03-29 to 2010-03-31",
						'amount' => "96.77",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest326()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "326",
			'name' => "Postpaid signup before billing start date, pro-rata, after included days",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-04-02",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
//FIXME
				/**
				'invoice_date' => "2010-04-18",
				'invoice_duedate' => "2010-04-18",
				*/
				// Invoice total without tax
				'invoice_subtotal' => "0",
				// Invoice tax amount
				'invoice_tax' => "0",
				'line_items' => array(
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest330()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "330",
			'name' => "Postpaid signup after billing period start date, after billing start date, pro-rata, after included days",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-04-22",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-04-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
//FIXME
				/**
				'invoice_date' => "2010-04-18",
				'invoice_duedate' => "2010-04-18",
				*/
				// Invoice total without tax
				'invoice_subtotal' => "0",
				// Invoice tax amount
				'invoice_tax' => "0",
				'line_items' => array(
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest331()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "331",
			'name' => "Postpaid signup after billing period start date, after billing start date, pro-rata, after included days, pay immediately",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-04-18",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-05-05",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
//FIXME
				/**
				'invoice_date' => "2010-04-18",
				'invoice_duedate' => "2010-04-18",
				*/
				// Invoice total without tax
				'invoice_subtotal' => "0",
				// Invoice tax amount
				'invoice_tax' => "0",
				'line_items' => array(
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest332()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "332",
			'name' => "Postpaid signup after billing period start date, before billing start date, pro-rata, after included days",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-04-18",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-06-10",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01"
			),
			'tests' => array(
//FIXME
				/**
				'invoice_date' => "2010-04-18",
				'invoice_duedate' => "2010-04-18",
				*/
				// Invoice total without tax
				'invoice_subtotal' => "0",
				// Invoice tax amount
				'invoice_tax' => "0",
				'line_items' => array(
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest340()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "340",
			'name' => "Postpaid6 signup after billing start date, pro-rata, after included days, pay immediately",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "quarterly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2009-08-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "6391.30",
				// Invoice tax amount
				'invoice_tax' => "894.78",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-07-01",
						'firstpaymentamount' => "6391.30",
						'description' => "Postpaid: 2009-12-01 to 2010-02-28",
						'amount' => "3000",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2009-08-20 to 2009-08-31",
						'amount' => "391.30",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2009-09-01 to 2009-11-30",
						'amount' => "3000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group postpaid
	 */
	public function testBillingTest350()
	{
		$cfields = self::$harness_product['fields'];

		$testcase = array(
			'id' => "350",
			'name' => "Postpaid6 signup after billing start date, pro-rata, after included days, pay immediately, billing end before cycle end",
			'order' => array(
				'product' => self::$harness_productid,
				'billingcycle' => "quarterly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2009-08-20",
				$cfields['billing_end'] => "2010-02-20",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01"
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "6091.30",
				// Invoice tax amount
				'invoice_tax' => "852.78",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-04-01",
						'firstpaymentamount' => "6091.30",
						'status' => "Cancelled",
						'description' => "Postpaid: 2009-12-01 to 2010-02-20",
						'amount' => "2700",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2009-08-20 to 2009-08-31",
						'amount' => "391.30",
						'taxed' => 1
					),
					array(
						'type' => "",
						'description' => "BackBill: 2009-09-01 to 2009-11-30",
						'amount' => "3000",
						'taxed' => 1
					)
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group variable
	 */
	public function testBillingTest400()
	{
		$cfields = self::$harness_product2['fields'];

		$testcase = array(
			'id' => "400",
			'name' => "Prepaid variable quantity",
			'order' => array(
				'product' => self::$harness_productid2,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-04-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01",
				// Custom data
				$cfields['variable_quantity'] => 3,
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "30",
				// Invoice tax amount
				'invoice_tax' => "4.20",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "30",
						'description' => "Prepaid: 2010-04-01 to 2010-04-30)\nVariable Calculation: 3 @10.00",
						'amount' => "30",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group variable
	 */
	public function testBillingTest402()
	{
		$cfields = self::$harness_product2['fields'];

		$testcase = array(
			'id' => "402",
			'name' => "Prepaid variable quantity, partial",
			'order' => array(
				'product' => self::$harness_productid2,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "prepaid",
				$cfields['billing_start'] => "2010-04-15",
				// Invoice date
				$cfields['billing_period_start'] => "2010-04-01",
				// Custom data
				$cfields['variable_quantity'] => 3,
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-03-31",
				// Invoice total without tax
				'invoice_subtotal' => "16",
				// Invoice tax amount
				'invoice_tax' => "2.24",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-05-01",
						'firstpaymentamount' => "16",
						'description' => "Prepaid: 2010-04-15 to 2010-04-30)\nVariable Calculation: 3 @10.00",
						'amount' => "16",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group variable
	 */
	public function testBillingTest404()
	{
		$cfields = self::$harness_product2['fields'];

		$testcase = array(
			'id' => "404",
			'name' => "Postpaid variable quantity, always round up",
			'order' => array(
				'product' => self::$harness_productid2,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-03-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01",
				// Custom data
				$cfields['variable_quantity'] => "3.1",
				$cfields['variable_attributes'] => "[round=alwaysup]",
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "40",
				// Invoice tax amount
				'invoice_tax' => "5.60",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "40",
						'description' => "Postpaid: 2010-03-01 to 2010-03-31)\nVariable Calculation: 4 @10.00",
						'amount' => "40",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group variable
	 */
	public function testBillingTest406()
	{
		$cfields = self::$harness_product2['fields'];

		$testcase = array(
			'id' => "406",
			'name' => "Postpaid variable quantity, always round down",
			'order' => array(
				'product' => self::$harness_productid2,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-03-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01",
				// Custom data
				$cfields['variable_quantity'] => "3.1",
				$cfields['variable_attributes'] => "[round=alwaysdown,unit=_users]",
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "30",
				// Invoice tax amount
				'invoice_tax' => "4.20",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "30",
						'description' => "Postpaid: 2010-03-01 to 2010-03-31)\nVariable Calculation: 3 users @10.00",
						'amount' => "30",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group variable
	 */
	public function testBillingTest408()
	{
		$cfields = self::$harness_product2['fields'];

		$testcase = array(
			'id' => "408",
			'name' => "Postpaid variable quantity, round (half) down",
			'order' => array(
				'product' => self::$harness_productid2,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-03-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01",
				// Custom data
				$cfields['variable_quantity'] => "3.5",
				$cfields['variable_attributes'] => "[round=down,unit=pcs]",
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "30",
				// Invoice tax amount
				'invoice_tax' => "4.20",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "30",
						'description' => "Postpaid: 2010-03-01 to 2010-03-31)\nVariable Calculation: 3pcs @10.00",
						'amount' => "30",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


	/**
	 * @group variable
	 */
	public function testBillingTest410()
	{
		$cfields = self::$harness_product2['fields'];

		$testcase = array(
			'id' => "410",
			'name' => "Postpaid variable quantity, round (half) up",
			'order' => array(
				'product' => self::$harness_productid2,
				'billingcycle' => "monthly",
			),
			'customfields' => array(
				// When service started on
				$cfields['signup_date'] => "2010-03-26",
				$cfields['billing_type'] => "postpaid",
				$cfields['billing_start'] => "2010-03-01",
				// Invoice date
				$cfields['billing_period_start'] => "2010-05-01",
				// Custom data
				$cfields['variable_quantity'] => "3.5",
				$cfields['variable_attributes'] => "[round=up,unit=Gbyte]",
			),
			'tests' => array(
				'invoice_date' => "2010-03-26",
				'invoice_duedate' => "2010-04-30",
				// Invoice total without tax
				'invoice_subtotal' => "40",
				// Invoice tax amount
				'invoice_tax' => "5.60",
				'line_items' => array(
					array(
						'type' => "Hosting",
						'regdate' => "2010-03-26",
						'nextduedate' => "2010-06-01",
						'firstpaymentamount' => "40",
						'description' => "Postpaid: 2010-03-01 to 2010-03-31)\nVariable Calculation: 4Gbyte @10.00",
						'amount' => "40",
						'taxed' => 1
					),
				)
			)
		);

		$this->_billingTest($testcase);
	}


}

// vim: ts=4
