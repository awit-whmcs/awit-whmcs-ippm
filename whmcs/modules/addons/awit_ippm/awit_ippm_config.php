<?php
/*
 * AWIT IPPM - Invoice Post Processing Module Config
 * Copyright (c) 2013-2016, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Make sure we are not being accessed directly
if (!defined("WHMCS"))
	die("This file cannot be accessed directly");



// Addon configuration
function _awit_ippm_config() {

	// Configuration
	$configarray = array(
			"name" => "AWIT IPPM",
			"description" => "This is an invoice post processing module including some utilities such as invoice item description and sales person / commission recording.",
			"version" => "0.4",
			"author" => "AllWorldIT",
			"language" => "english",
			"fields" => array(
				// Invoice include days - Global setting
				"invoice_include_days" => array (
						"FriendlyName" => "Invoice Include Days",
						"Description" => "If invoice is made out between DueDate - InvoiceIncludeDays, the service period will be billed if the invoice is generated between that and the due date.",
						"Type" => "text", "Size" => "5",
						"Default" => "5"
				),

				// Sales Person - Custom field
				"sales_person" => array (
						"FriendlyName" => "Sales Person Custom Field",
						"Description" => "Sales person custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Sales Person",

						// Custom field type
						"awitippm_ctype" => "text"
				),

				// Sales Commission - Custom field
				"sales_commission" => array (
						"FriendlyName" => "Sales Commission Custom Field",
						"Description" => "Sales commission custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Sales Commission",

						"awitippm_ctype" => "text"
				),
				// Sales Commission Structure - Global setting
				"sales_commission_s" => array (
						"FriendlyName" => "Sales Commission Structure",
						"Description" => "Sales commission structure (eg. A=50,B=30,C=20,D=10)",
						"Type" => "text", "Size" => "80",
						"Default" => "A=50,B=30,C=20,D=15,E=10,F=7.5,G=5,H=2.5"
				),

				// Signup Date - Custom field
				"signup_date" => array (
						"FriendlyName" => "Signup Date Field",
						"Description" => "Signup date custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Signup Date",

						"awitippm_ctype" => "date"
				),
				// Billing Type - Custom field
				"billing_type" => array (
						"FriendlyName" => "Billing Type Custom Field",
						"Description" => "Billing type custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Billing Type",

						"awitippm_ctype" => "text"
				),
				// Billing Start - Custom field
				"billing_start" => array (
						"FriendlyName" => "Billing Start Custom Field",
						"Description" => "Billing start custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Billing Start",

						"awitippm_ctype" => "date"
				),
				// Billing End - Custom field
				"billing_end" => array (
						"FriendlyName" => "Billing End Custom Field",
						"Description" => "Billing end custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Billing End",

						"awitippm_ctype" => "date"
				),
				// Billing Period Start - Custom field
				"billing_period_start" => array (
						"FriendlyName" => "Billing Period Start Custom Field",
						"Description" => "Billing period start. This will override nextduedate one time only.",
						"Type" => "text", "Size" => "30",
						"Default" => "Billing Period Start",

						"awitippm_ctype" => "date"
				),

				// Variable Quantity - Custom field
				"variable_quantity" => array (
						"FriendlyName" => "Variable Quantity Custom Field",
						"Description" => "Variable quantity custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Variable Quantity",

						"awitippm_ctype" => "text"
				),
				// Variable Amount - Custom field
				"variable_amount" => array (
						"FriendlyName" => "Variable Amount Custom Field",
						"Description" => "Variable amount custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Variable Amount",

						"awitippm_ctype" => "text"
				),
				// Variable Identifier - Custom field
				"variable_identifier" => array (
						"FriendlyName" => "Variable Identifier Custom Field",
						"Description" => "Variable identifier custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Variable Identifier",

						"awitippm_ctype" => "text"
				),
				// Variable Attributes - Custom field
				"variable_attributes" => array (
						"FriendlyName" => "Variable Attributes Custom Field",
						"Description" => "Variable Attributes custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Variable Attributes",

						"awitippm_ctype" => "text"
				),

				// Contract Start - Custom field
				"contract_start" => array (
						"FriendlyName" => "Contract Start Custom Field",
						"Description" => "Contract start custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Contract Start",

						"awitippm_ctype" => "date"
				),
				// Contract Term - Custom field
				"contract_term" => array (
						"FriendlyName" => "Contract Term Custom Field",
						"Description" => "Contract term custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Contract Term",

						"awitippm_ctype" => "date"
				),

				// Commitment Value - Custom field
				"commitment_value" => array (
						"FriendlyName" => "Commitment Value Custom Field",
						"Description" => "Commitment Value custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Commitment Value",

						"awitippm_ctype" => "date"
				),
				// Commitment Calculation - Custom field
				"commitment_calculation" => array (
						"FriendlyName" => "Commitment Calculation Custom Field",
						"Description" => "Commitment Calculation custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Commitment Calculation",

						"awitippm_ctype" => "date"
				),

				// Bulk Discount Calculation - Custom field
				"bulk_discount_calculation" => array (
						"FriendlyName" => "Bulk Discount Calculation Custom Field",
						"Description" => "Bulk Discount Calculation custom field name",
						"Type" => "text", "Size" => "30",
						"Default" => "Bulk Discount Calculation",

						"awitippm_ctype" => "date"
				),
			)
	);

	return $configarray;
}



// vim: ts=4
