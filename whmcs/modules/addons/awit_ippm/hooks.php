<?php
/*
 * AWIT IPPM - Hooks
 * Copyright (c) 2013-2016, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



// Make sure we are not being accessed directly
if (!defined("WHMCS"))
	die("This file cannot be accessed directly");

if (!defined('AWIT_IPPM_LOG_FILE')) {
	define('AWIT_IPPM_LOG_FILE', '/tmp/awit_ippm_hook2.log');
}

if (!defined('AWIT_IPPM_LOG_LEVEL')) {
	define('AWIT_IPPM_LOG_LEVEL', 5); // 0 = no logging, 1 = unit test output, 2 = min detail, 5 = max detail
}



require_once('awit_ippm_common.php');



add_hook("AdminAreaHeadOutput", 5, "awit_ippm_hook_AdminAreaHeadOutput");

add_hook("InvoiceCreationPreEmail",5,"awit_ippm_hook_InvoiceCreationPreEmail");

// vim: ts=4
