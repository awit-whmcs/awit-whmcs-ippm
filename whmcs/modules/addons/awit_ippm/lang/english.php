<?php

$_ADDONLANG['intro'] = "AWIT Invoice Post Processing Module";
$_ADDONLANG['description'] = "The awit_ippm module enables the configuration of sales staff and commission percentages to be recorded as well as the addition of invoice item descriptions based on custom fields.";
$_ADDONLANG['documentation'] = "Pending..";

?>
